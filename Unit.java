public interface Unit {
	public double convertTo(Unit unit, double amt);
	public double getValue();
	public String toString();
}