import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A Graphical User Interface that receives input from user and
 * output desired length unit.
 * @author Pakpon Jetapai
 */
public class ConverterUI extends JFrame implements Runnable {
	private JButton convertButton;
	private JButton clearButton;
	private UnitConverter unitconverter;
	private JTextField inputField1;
	private JTextField inputField2;
	private JComboBox<Unit> unitBox1;
	private JComboBox<Unit> unitBox2;
	private JLabel equalLabel;
	/**
	 * Initialize ConverterUI and it's components. 
	 * @param uc a UnitConverter that do the convention before
	 * the result will be put on UI.
	 */
	public ConverterUI( UnitConverter uc ) {
		super("Distance Converter");
		this.unitconverter = uc;;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents(unitconverter);
		this.pack();
	}
	private void initComponents(UnitConverter uc) {
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout();
		contents.setLayout( layout );
		convertButton = new JButton("Convert!");
		ActionListener listener = new ConvertButtonListener();
		convertButton.addActionListener(listener);
		clearButton = new JButton("Clear");
		clearButton.addActionListener(new ClearButtonListener());
		inputField1 = new JTextField(10);
		inputField1.addKeyListener(new ConvertOnEnterListenter());
		inputField2 = new JTextField(10);
		inputField2.setEditable(false);
		equalLabel = new JLabel("=");
		unitBox1 = new JComboBox<Unit>(uc.getUnits());
		unitBox2 = new JComboBox<Unit>(uc.getUnits());
		contents.add( inputField1 );
		contents.add( unitBox1 );
		contents.add( equalLabel );
		contents.add( inputField2 );
		contents.add( unitBox2 );
		contents.add( convertButton );
		contents.add( clearButton );
	}
	@Override
	public void run() {
		this.setVisible(true);
	}
	/**
	 * A listener that activates when button is clicked, converts
	 * ,and shows the result.  
	 * @author Pakpon Jetapai
	 */
	class ConvertButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String s = inputField1.getText().trim();
			double value = 0.0;
			double result = 0.0;
			try {
				value = Double.valueOf( s );
				result = unitconverter.convert( value, (Unit) unitBox1.getSelectedItem(), (Unit) unitBox2.getSelectedItem());
				inputField2.setText(String.valueOf(result));
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(new JFrame(), "Your did not in put a number");
			} 
		}
	}
	/**
	 * A listener that activates upon pressing ENTER, converts, and shows on he UI.
	 * @author Pakpon Jetapai
	 *
	 */
	class ConvertOnEnterListenter implements KeyListener {
		@Override
		public void keyTyped(KeyEvent e) {	}
		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode()==KeyEvent.VK_ENTER) {
				String s = inputField1.getText().trim();
				double value = 0.0;
				double result = 0.0;
				try {
					value = Double.valueOf( s );
					result = unitconverter.convert( value, (Unit) unitBox1.getSelectedItem(), (Unit) unitBox2.getSelectedItem());
					inputField2.setText(String.valueOf(result));
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(new JFrame(), "Your did not in put a number or a positive number");
				} 
			}
		}
		@Override
		public void keyReleased(KeyEvent e) {	}
	}
	/**
	 * A listener that clear any input and output on the UI.
	 * @author Pakpon Jetapai
	 *
	 */
	class ClearButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			inputField1.setText("");
			inputField2.setText("");
		}
	}
}
