/**
 * A class that receives inputs from UI, converts a value from
 * one length unit to another, and shows the result in UI.
 * @author Pakpon Jetapai
 */
public class UnitConverter {
	/**
	 * Convert  amount of a length unit to another by using Length enum.
	 * @param amount is a value of current length unit.
	 * @param fromUnit is a current unit.
	 * @param toUnitis a unit that amount will be converted to.
	 * @return value a unit resulted from converting length unit.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit) {
		double value = fromUnit.convertTo(toUnit, amount);
		return value;
	}
	/**
	 * Get every unit in the Length enum.
	 * @return array of units in Length enum.
	 */
	public Unit[] getUnits() {
		return Length.values();
	}
}
