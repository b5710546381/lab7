
public class Main {
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter();
		ConverterUI cui = new ConverterUI(uc);
		cui.run();
	}
}
