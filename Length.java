/**
 * A class that holds various length units and allows
 * converting a value from one length unit to another. 
 * </br>NOTE: Meter is a base length unit.
 * @author Pakpon Jetapai
 */
public enum Length implements Unit {
	METER( "Meter" , 1.0 ),
	CENTIMTER( "Centimeter", 0.01 ),
	KILOMETER( "Kilometer" , 1000.0 ),
	MILE( "Mile" , 1609.344 ),
	FOOT( "Foot" , 0.30480 ),
	WA( "Wa", 2.0 );
	private String name;
	private double value;
	/**
	 * Initialize Length unit, name and value.
	 * @param name is a word representing the length unit.
	 * @param value is a multiplier to convert from a length 
	 * unit to base length unit or from the base unit to
	 * a desired length unit.
	 */
	Length (String name, double value) {
		this.name = name;
		this.value = value;
	}
	/**
	 * Convert amount from current unit to desired length unit.
	 * @param unit is a length unit that amount will be converted to.
	 * @param amount a value of current length unit that will be changed.
	 */
	public double convertTo(Unit unit, double amount) {
		amount *= this.getValue();
		amount /= unit.getValue();
		return amount;
	}
	/**
	 * Get the value of current length unit.
	 * @return value of the unit.
	 */
	public double getValue() {
		return this.value;
	}
	/**
	 * Get the name of current length unit.
	 * @return the name of the unit that will be shown on the UI.
	 */
	public String toString() {
		return this.name;
	}
}
